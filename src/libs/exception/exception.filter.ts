import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { HttpAdapterHost } from '@nestjs/core';
import { BaseException } from './base.exception';
import { ExceptionMapper } from './exception.mapper';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(HttpExceptionFilter.name);
  constructor(
    private readonly httpAdapterHost: HttpAdapterHost,
    private readonly exceptionMapper: ExceptionMapper,
  ) {}
  catch(exception: unknown, host: ArgumentsHost) {
    let mappedException: HttpException;
    if (exception instanceof HttpException) {
      this.logger.debug(
        `Nest exception captured: ${exception.constructor.name}`,
      );
      mappedException = this.exceptionMapper.standardizeException(exception);
    } else if (exception instanceof BaseException) {
      this.logger.debug(
        `Operational exception captured: ${exception.constructor.name}`,
      );
      mappedException = this.exceptionMapper.toHttpException(exception);
    } else if (exception instanceof Error) {
      this.logger.warn(
        `Unexpected exception captured with message: ${exception.message}`,
      );
      this.logger.debug(
        `Unexpected exception captured with stack: ${exception.stack}`,
      );
      mappedException =
        this.exceptionMapper.toInternalServerException(exception);
    }
    const ctx = host.switchToHttp();
    const { httpAdapter } = this.httpAdapterHost;
    httpAdapter.reply(
      ctx.getResponse(),
      mappedException.getResponse(),
      mappedException.getStatus(),
    );
  }
}
