/**
 * Base class for custom exceptions.
 *
 * @abstract
 * @class BaseException
 * @extends {Error}
 */
export abstract class BaseException extends Error {
  constructor(readonly message: string) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
  }
  abstract code: string;
}
