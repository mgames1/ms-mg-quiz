export const responseLogger = (res) => {
  return `Request completed: ${res.statusCode} ${buildHeaders(res)}`;
};

const buildHeaders = (res) => {
  const headers = JSON.stringify(res.getHeaders());
  return `with headers ${headers}`;
};
