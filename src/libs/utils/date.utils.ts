import { format } from 'date-fns';

export class DateUtils {
  private static readonly format: string = 'dd-MM-yyyy HH:mm:ss.SSS';
  public static now(): string {
    return format(new Date(), this.format);
  }
}
