import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerModule } from 'nestjs-pino';
import { pinoParams } from './libs/infraestructure/loggers/pino.params';
import { TraceMiddleware } from './libs/infraestructure/http/trace/trace.middleware';
import { HttpModule } from '@nestjs/axios';
import { httpOptions } from './libs/infraestructure/http/http.options';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeormOptions } from './libs/infraestructure/orm/orm.options';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionFilter } from './libs/exception/exception.filter';
import { ExceptionMapper } from './libs/exception/exception.mapper';

@Module({
  imports: [
    LoggerModule.forRoot(pinoParams),
    TypeOrmModule.forRoot(typeormOptions),
    HttpModule.registerAsync(httpOptions),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    ExceptionMapper,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(TraceMiddleware).forRoutes('*');
  }
}
