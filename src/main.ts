import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from 'nestjs-pino';
//https://github.com/helmetjs/helmet/issues/348
import helmet = require('helmet');
import { corsOptions } from './libs/infraestructure/http/cors/cors.options';
import { ValidationPipe } from '@nestjs/common';
import { validationOptions } from './libs/infraestructure/validation/validation.options';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.enableCors(corsOptions);
  app.use(helmet());
  app.useGlobalPipes(new ValidationPipe(validationOptions));
  app.useLogger(app.get(Logger));
  await app.listen(3000);
}
bootstrap();
