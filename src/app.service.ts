import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom, Observable } from 'rxjs';
import { AxiosResponse } from 'axios';

@Injectable()
export class AppService {
  constructor(private httpService: HttpService) {}
  postTests(): Promise<AxiosResponse<any>> {
    return firstValueFrom(this.httpService.get('http://localhost:3000/test'));
  }
}
