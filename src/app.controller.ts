import { Body, Controller, Logger, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { IsNotEmpty, IsNotEmptyObject, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class InnerPassword {
  @IsNotEmpty()
  password: string;
}

export class CreateUserDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  password: string;

  @ValidateNested()
  @Type(() => InnerPassword)
  inner: InnerPassword;
}

@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name);
  constructor(private appService: AppService) {}

  @Post('/users')
  postUsers(@Body() createUserDto: CreateUserDto): string {
    this.logger.warn('WARN');
    this.logger.error('ERROR');
    return 'asd';
  }

  @Post('/hello')
  getHello(): string {
    this.logger.warn('WARN');
    this.logger.error('ERROR');
    return 'Hello World!';
  }
}
